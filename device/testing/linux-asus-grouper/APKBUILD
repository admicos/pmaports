# Maintainer: David Heidelberg <david@ixit.cz>
_flavor=asus-grouper
_commit="fadacdcb9e5b60f6547c37f519d1851cb2f90edb"
_config="config-$_flavor.armv7"

pkgname=linux-asus-grouper
pkgver=5.6.0_rc2
pkgrel=1
arch="armv7"
pkgdesc="Nexus 7 grouper/tilapia (2012) kernel fork, close to mainline"
url="https://postmarketos.org"
makedepends="perl sed installkernel bash gmp-dev bc linux-headers elfutils-dev
	     devicepkg-dev bison flex openssl-dev xz findutils"
options="!strip !check !tracedeps pmb:cross-native"
source="
	$pkgname-$pkgver-$_commit.tar.gz::https://github.com/okias/linux/archive/$_commit.tar.gz
	$_config
"
license="GPL-2.0-only"

_carch="arm"

_ksrcdir="$srcdir/linux-$_commit"

prepare() {
	mkdir -p "$srcdir"/build
	cp -v "$srcdir"/$_config "$srcdir"/build/.config
	make -C "$_ksrcdir" O="$srcdir"/build ARCH="$_carch" \
		olddefconfig
}

build() {
	cd "$srcdir"/build
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-Alpine"
}

package() {
	install -Dm644 "$srcdir/build/arch/$_carch/boot/"*zImage \
		"$pkgdir/boot/vmlinuz-$_flavor"

	install -D "$srcdir/build/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"

	cd "$srcdir"/build

	local _install
	case "$CARCH" in
	aarch64*|arm*)	_install="modules_install dtbs_install" ;;
	*)		_install="modules_install" ;;
	esac

	make -j1 $_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"
}
sha512sums="22cdafad7888f3267fbd92cdb46d67390c89c9e2b433faaef591ca576bc88b4f51157495948a049f1be6b98a6eacc71ed7253033e98bc708a35e3c62f4f4cf90  linux-asus-grouper-5.6.0_rc2-fadacdcb9e5b60f6547c37f519d1851cb2f90edb.tar.gz
293290400eb1d30fd381006290ee891f1cb369ee2ad090ba6fc1e90005ecb6c555d763f1c600867a7832d414e8f7c51e195981fc9eba290f87fad9411b80799a  config-asus-grouper.armv7"
