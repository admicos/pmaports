# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=postmarketos-ui-phosh
pkgver=4
pkgrel=1
pkgdesc="(Wayland) Mobile UI developed for the Librem 5 (works only with numeric passwords!)"
url="https://puri.sm"
arch="noarch !armhf !x86" # x86: aports#11807
license="GPL-3.0-or-later"
depends="bluez
	gnome-keyring
	iio-sensor-proxy
	lightdm
	lightdm-openrc
	phosh
	polkit-elogind
	pulseaudio
	"
_pmb_recommends="calls
	chatty
	firefox-esr
	gedit
	gnome-calculator
	gnome-clocks
	gnome-contacts
	gnome-software
	kgx
	postmarketos-artwork-wallpapers
	postmarketos-config-firefox
	postmarketos-hidden-desktop-entries
	postmarketos-welcome-gtk3
	xdg-user-dirs
	xorg-server-xwayland
	"
subpackages="$pkgname-qt_tweaks"
install="$pkgname.post-install $pkgname.post-upgrade"
source="60-lightdm-autologin.conf
	000-gschema.override
	osk.sh
	gtk-app-wayland.sh
	qt-tweaks.sh
	dconf-profile-postmarketos
	mimeapps.list
	01-phoc-scaling
	02-gnome-software-no-first-run
	"
options="!check"

package() {
	install -Dm644 "$srcdir"/60-lightdm-autologin.conf \
		"$pkgdir"/usr/share/lightdm/lightdm.conf.d/60-autologin.conf
	install -Dm644 "$srcdir"/000-gschema.override \
		"$pkgdir"/usr/share/glib-2.0/schemas/000-postmarketos.gschema.override
	install -Dm755 "$srcdir"/gtk-app-wayland.sh \
		"$pkgdir"/etc/profile.d/gtk-app-wayland.sh
	install -Dm755 "$srcdir"/osk.sh \
		"$pkgdir"/usr/bin/osk-wayland
	install -Dm755 "$srcdir"/dconf-profile-postmarketos \
		"$pkgdir"/etc/dconf/profile/user
	install -Dm644 "$srcdir"/mimeapps.list \
		"$pkgdir"/usr/share/applications/mimeapps.list
	install -Dm755 "$srcdir"/01-phoc-scaling \
		"$pkgdir"/etc/dconf/db/postmarketos.d/01-phoc-scaling
	install -Dm755 "$srcdir"/02-gnome-software-no-first-run \
		"$pkgdir"/etc/dconf/db/postmarketos.d/02-gnome-software-no-first-run
}

qt_tweaks() {
	source="qt-tweaks.sh"
	install_if="$pkgname qt5-qtbase"
	depends="qt5-qtwayland"
	install -Dm755 "$srcdir"/qt-tweaks.sh \
		"$subpkgdir"/etc/profile.d/phosh-qt-tweaks.sh
}

sha512sums="c51a5cf93495069d71900dd1c6fbddfa3bdfb70dbab8141d1b115b6150975a12206c63517f18dc516e0366028b32e014b6d37a20e19ff73d5f0934c518d3ab39  60-lightdm-autologin.conf
10075ef585527e2b2a47a9aba02c8089fabb099992c4cd7d634f614d3e40e9ee918803277d5534d44e7e838d6a8fa2bae43ed146c693c2ff9a1b5619f6d8c2da  000-gschema.override
4113ef59267e88d205ef1e1aec0ed11ccf817a25c232f2006a538b56fb466fad5025ad445d109e367ca92ee98d9b25f1f9a1a4b9bae2cb80df12a3739d62d10a  osk.sh
57793bb079c76ba3bdcfca7880aa887de11fe80e7a05557b78435e57feabab70fbaeedc42da3aec6f914c35bba8e8ee9918367516eb45ee247f63f694624f179  gtk-app-wayland.sh
6e193eca3961a78d47b4656892eae34d019d9317a255a201f5ea61e3300caff04c526a27cd98d0edc072b36e3eaf3a1768f4cd27c5e2be8b19c167d535c820a6  qt-tweaks.sh
e00756c2c056f68123d877f2f6a5ad3434ca7851095f021c26831c081728b821cf7947ba08d6742eee51d93dc83859a7ead553f4dcbc8b6fcefa33ae344ba178  dconf-profile-postmarketos
5f30d69d5d2c419dc328c44b5b897fc10e21d58ca1261129b2376dfb7b4919d525f62b39806582262591759d578b8a37e1103453bbf79a1f2df2f5d411221f74  mimeapps.list
6d99bd718fece9d202ebf76b807335b1e31ebeca52f2b8870bb4772787cf76bf4602f2506bc4ed077da0eac584ec72b879ad8317c926aabcf2ed48b0d7ba48f7  01-phoc-scaling
834d1c558b6a9ca26345928895a5436919aab13f944410240516953a13f8fdafae995af4b1baeee560a800c405a4d67c44bb92499bf8cacae7a17f6d9893577e  02-gnome-software-no-first-run"
